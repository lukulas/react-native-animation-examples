import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Timing from './pages/Timing';
import SequenceInterpolate from './pages/SequenceInterpolate';
import DragNDrop from './pages/DragNDrop';
import Home from './pages/Home';

const Routes = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ title: 'AnimationExamples' }}
      />
      <Stack.Screen name="Timing" component={Timing} />
      <Stack.Screen
        name="SequenceInterpolate"
        component={SequenceInterpolate}
      />
      <Stack.Screen name="DragNDrop" component={DragNDrop} />
    </Stack.Navigator>
  );
};

export default Routes;
