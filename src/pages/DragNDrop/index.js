import React, { useRef } from 'react';
import { View, Animated, StyleSheet, PanResponder } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
  },
  span: {
    backgroundColor: '#7159c1',
    height: 70,
    width: 70,
    borderRadius: 35,
  },
});

const DragNDrop = () => {
  const pan = useRef(new Animated.ValueXY()).current;
  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: (e, gestureState) => true,
      onPanResponderMove: Animated.event([null, { dx: pan.x, dy: pan.y }]),
      onPanResponderGrant: (e, gestureHandler) => {
        pan.setOffset({
          x: pan.x._value,
          y: pan.y._value,
        });
      },
      onPanResponderRelease: () => {
        pan.flattenOffset();
      },
    }),
  ).current;

  return (
    <View style={styles.container}>
      <Animated.View
        {...panResponder.panHandlers}
        style={[
          styles.span,
          {
            transform: [{ translateX: pan.x }, { translateY: pan.y }],
          },
        ]}
      />
    </View>
  );
};

export default DragNDrop;
