import React, { useEffect, useState } from 'react';

import { View, StyleSheet, Animated, Text } from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    flex: 1,
    padding: 30,
    flexDirection: 'row',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    height: 70,
    width: 70,
    backgroundColor: '#7159c1',
    borderRadius: 8,
  },
  containerText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

const Timing = () => {
  const [timingY, setBallY] = useState(new Animated.Value(0));
  const [springY, setSquareY] = useState(new Animated.Value(0));
  const [decayY, setDecayY] = useState(new Animated.Value(0));

  useEffect(() => {
    Animated.loop(
      Animated.timing(timingY, {
        toValue: 400,
        duration: 2000,
      }),
    ).start();

    Animated.loop(
      Animated.spring(springY, {
        toValue: 400,
        bounciness: 25,
      }),
    ).start();

    Animated.loop(
      Animated.decay(decayY, {
        velocity: 0.8,
      }),
    ).start();
  });

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.button, { top: timingY }]}>
        <Text style={styles.containerText}>timing</Text>
      </Animated.View>
      <Animated.View style={[styles.button, { top: springY }]}>
        <Text style={styles.containerText}>spring</Text>
      </Animated.View>
      <Animated.View style={[styles.button, { top: decayY }]}>
        <Text style={styles.containerText}>decay</Text>
      </Animated.View>
    </View>
  );
};

export default Timing;
