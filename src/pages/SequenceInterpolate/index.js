import React, { useEffect, useState } from 'react';
import { View, Animated, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
  },
  button: {
    backgroundColor: '#7159c1',
    width: 70,
    height: 70,
    borderRadius: 0,
  },
  containerText: {
    fontWeight: 'bold',
    color: '#fff',
  },
});

const SequenceInterpolate = () => {
  const [y, setY] = useState(new Animated.Value(0));
  const [x, setX] = useState(new Animated.Value(0));

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.spring(y, {
          toValue: 400,
          bounciness: 25,
        }),
        Animated.decay(x, {
          velocity: 0.6,
        }),
        Animated.timing(y, {
          toValue: 0,
          duration: 2000,
        }),
        Animated.spring(x, {
          toValue: 0,
          bounciness: 15,
        }),
      ]),
    ).start();
  });

  return (
    <View style={styles.container}>
      <Animated.View
        style={[
          styles.button,
          {
            top: y,
            left: x,
            opacity: y.interpolate({
              inputRange: [0, 400],
              outputRange: [1, 0.3],
            }),
            borderRadius: x.interpolate({
              inputRange: [0, 300],
              outputRange: [0, 35],
            }),
          },
        ]}
      />
    </View>
  );
};

export default SequenceInterpolate;
