import React, { useState } from 'react';
import { View, FlatList, Button } from 'react-native';

const Home = ({ navigation }) => {
  const [views, setViews] = useState([
    {
      id: 1,
      viewTitle: 'timing, sprint',
      viewName: 'Timing',
    },
    {
      id: 2,
      viewTitle: 'Sequency and Interpolate',
      viewName: 'SequenceInterpolate',
    },
    { id: 3, viewTitle: 'Drag and Drop', viewName: 'DragNDrop' },
  ]);

  return (
    <View>
      <FlatList
        data={views}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <Button
            title={item.viewTitle}
            onPress={() => {
              navigation.navigate(item.viewName);
            }}
          />
        )}
      />
    </View>
  );
};

export default Home;
